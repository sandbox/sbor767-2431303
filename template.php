<?php
/**
 * @file
 * Theme functions for "Lazy" theme.
 */

/**
 * Implements hook_theme().
 */
function lazy_theme() {
  return array(
    'lazy_counter_form_adjust' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Theme function for 'lazy_counter_form_adjust'.
 */
function lazy_lazy_counter_form_adjust($variables) {

  $form = $variables['form'];

  $output = '';
  $output .= '<table class="lazy-table"><tbody><tr>';
  $output .= '<td>' . drupal_render($form['lazy_counter_reset_period']) . '</td>';
  $output .= '<td>' . drupal_render($form['lazy_counter_prize_level']) . '</td>';
  $output .= '</tr></tbody></table>';

  $output .= drupal_render_children($form);

  return $output;
}
